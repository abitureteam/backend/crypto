﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Abiture.Crypto
{
    public static class SHA512
    {
        /// <summary>
        /// Hash
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        /// <exception cref="System.Reflection.TargetInvocationException">Ignore</exception>
        /// <exception cref="ObjectDisposedException">Ignore</exception>
        public static string Hash(string s)
        {
            if (string.IsNullOrEmpty(s))
                if (s == null)
                    throw new ArgumentNullException(nameof(s));
                else
                    throw new ArgumentException($"Given parameter {nameof(s)} cannot be empty");

            using (var hasher = System.Security.Cryptography.SHA512.Create())
            {
                //Convert String to ByteArray, then hash. Returns ByteArray.
                var hash = hasher.ComputeHash(Encoding.UTF8.GetBytes(s));

                //Convert ByteArray to hexdec string
                StringBuilder sb = new StringBuilder(128);
                foreach (byte b in hash)
                {
                    //Add as HexDec
                    sb.Append(b.ToString("X2"));
                }
                return sb.ToString();
            }
        }

        /// <summary>
        /// Generate a Salt-Hash Pair from a given input s.
        /// </summary>
        /// <param name="s">This is the value to be hashed. Most likely this is a password.</param>
        /// <returns>A tuple. First value is the salt, second is the hash</returns>
        /// <exception cref="CryptographicException"></exception>
        public static Tuple<string, string> GenerateSaltHashPair(string s)
        {
            if (string.IsNullOrEmpty(s))
                if (s == null)
                    throw new ArgumentNullException(nameof(s));
                else
                    throw new ArgumentException($"Given parameter {nameof(s)} cannot be empty");


            string randomString = GenerateRandomString(64);
            return new Tuple<string, string>(randomString, Hash(randomString + s));
        }

        /// <summary>
        /// Generate a random hexadecimal string
        /// </summary>
        /// <param name="bytes">the number of bytes the returned string represents</param>
        /// <returns>a hexadecimal string of length bytes*2</returns>
        /// <exception cref="CryptographicException"></exception>
        private static string GenerateRandomString(uint bytes)
        {
            byte[] byteStream = new byte[bytes];
            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(byteStream);
            }
            StringBuilder sb = new StringBuilder(128);
            foreach (byte b in byteStream)
            {
                sb.Append(b.ToString("X2"));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Verify, that a Password belongs to a given Salt-Hash-Pair
        /// </summary>
        /// <param name="Password">The password as string</param>
        /// <param name="SaltHashPair">The Salt-Hash-Pair. First value is a salt, second is the hash</param>
        /// <returns></returns>
        public static bool VerifyPassword(string Password, Tuple<string, string> SaltHashPair)
        {
            return Hash(SaltHashPair.Item1 + Password) == SaltHashPair.Item2;
        }
    }
}