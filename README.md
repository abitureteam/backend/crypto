This package is used to hash passwords for the Abiture.org website.

It can create a Salt-Hash Pair from an input and verify a password.