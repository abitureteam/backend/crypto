using NUnit.Framework;
using System;

namespace Abiture.Crypto.Test
{
    public class SHA512Test
    {
        #region SHA512.Hash()

        [TestCase("bla", "6ba028366ebcdcef1ce3d73883c3475def4b7925f80e800bc82d91aa4430093622f9f95d20894022864dfa55d9c901ff520e070132eafabdae62a73e5ceeaed1")]
        [TestCase("asdf", "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429080fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1")]
        [TestCase("password", "b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86")]
        public void HashedInputShouldMatchOutput(string _in, string _out)
        {
            Assert.AreEqual(_out.ToUpper(), SHA512.Hash(_in));
        }

        [Test]
        public void NullHashInputShouldThrowArgumentException()
        {
            Assert.Throws<ArgumentNullException>(() => SHA512.Hash(null));
        }

        [Test]
        public void EmptyHashInputShouldThrowArgumentException()
        {
            Assert.Throws<ArgumentException>(() => SHA512.Hash(string.Empty));
        }

        #endregion SHA512.Hash()

        #region SHA512.GenerateSaltHashPair()

        [TestCase("asdf")]
        [TestCase("asklsdaklsadd")]
        public void GeneratedPepperHashPairShouldMatchInputPassword(string password)
        {
            var PepperHashPair = SHA512.GenerateSaltHashPair(password);
            Assert.IsTrue(SHA512.VerifyPassword(password, PepperHashPair));
        }

        [Test]
        public void NullPasswordInputShouldThrowArgumentException()
        {
            Assert.Throws<ArgumentNullException>(() => SHA512.GenerateSaltHashPair(null));
        }

        [Test]
        public void EmptyPasswordInputShouldThrowArgumentException()
        {
            Assert.Throws<ArgumentException>(() => SHA512.GenerateSaltHashPair(string.Empty));
        }

        #endregion SHA512.GenerateSaltHashPair()
    }
}